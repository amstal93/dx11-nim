FROM ubuntu:eoan
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get install -y dbus-x11 procps psmisc && \
    apt-get install -y xdg-utils xdg-user-dirs menu-xdg mime-support desktop-file-utils && \
    apt-get install -y mesa-utils mesa-utils-extra libxv1

# Language/locale settings
#   replace en_US by your desired locale setting, 
#   for example de_DE for german.
ENV LANG en_US.UTF-8
RUN echo $LANG UTF-8 > /etc/locale.gen && \
    apt-get install -y locales && \
    update-locale --reset LANG=$LANG

# Mate desktop core
RUN apt-get install -y --no-install-recommends mate-desktop-environment-core \
	mate-applets appmenu-gtk2-module ca-certificates nautilus cryptsetup fonts-dejavu-core fonts-freefont-ttf \
	libmtp-runtime mate-icon-theme mate-indicator-applet mate-media mate-menus \
	mate-notification-daemon mate-polkit mate-settings-daemon terminator mate-utils \
	ubuntu-mate-core ubuntu-mate-default-settings xdg-user-dirs-gtk xkb-data \
	xorg zenity

# additional goodies
RUN apt-get install -y --no-install-recommends fonts-liberation gnome-keyring \
	gnome-menus indicator-application indicator-datetime indicator-messages indicator-notifications \
	indicator-power indicator-session laptop-detect mate-applet-appmenu mate-applet-brisk-menu \
	mate-calc mate-dock-applet mate-hud mate-menu mate-sensors-applet mate-system-monitor \
	mate-sntray-plugin mate-themes mate-tweak mate-window-buttons-applet mate-window-menu-applet \
	mate-window-title-applet network-manager network-manager-gnome network-manager-openvpn-gnome \
	network-manager-pptp-gnome plank policykit-desktop-privileges qt5-gtk-platformtheme qt5-style-platform-gtk2 \
	redshift-gtk smbclient tilda ubuntu-mate-artwork breeze-cursor-theme

# CORE COMPONENTS
RUN apt update && apt -y dist-upgrade && \
	apt install -y gnupg vim curl wget git build-essential fish net-tools dnsutils \
	netcat apt-utils iputils-ping iproute2 telnet zip unzip tmux && \
	apt -y autoremove --purge && apt -y clean

# ADDITIONAL APPS
RUN curl -s https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > /root/microsoft.gpg && \
	install -o root -g root -m 644 /root/microsoft.gpg /etc/apt/trusted.gpg.d/ && rm /root/microsoft.gpg && \
	echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list && \
	apt update && apt install -y firefox code gnome-terminal gnome-logs file-roller evince eog gitg htop keepass2 telegram-desktop gedit nitroshare && \
	curl --remote-name https://prerelease.keybase.io/keybase_amd64.deb && \
	apt -y install ./keybase_amd64.deb && \
	rm keybase_amd64.deb && \
	apt -y autoremove --purge && apt -y clean

RUN apt install -y apt-transport-https curl && \
	curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add - && \
	echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main" | tee /etc/apt/sources.list.d/brave-browser-release.list && \
	apt update && apt install -y brave-browser

# GOLANG

ARG GO_VERSION=1.13.4
ENV GO_VERSION=$GO_VERSION

RUN curl -O https://dl.google.com/go/go${GO_VERSION}.linux-amd64.tar.gz \
    && tar xf go${GO_VERSION}.linux-amd64.tar.gz \
    && rm go${GO_VERSION}.linux-amd64.tar.gz \
    && mv go /opt \
    && ln -s /opt/go/bin/go /usr/bin \
    && ln -s /opt/go/bin/gofmt /usr/bin
 
# NODEJS

ARG NODE_VERSION=v12.13.1
ENV NODE_VERSION=$NODE_VERSION

RUN curl -O https://nodejs.org/dist/${NODE_VERSION}/node-${NODE_VERSION}-linux-x64.tar.xz \
    && tar -xf node-${NODE_VERSION}-linux-x64.tar.xz -C /opt/ \
    && ln -s /opt/node-${NODE_VERSION}-linux-x64/bin/npm /usr/bin/ \
    && ln -s /opt/node-${NODE_VERSION}-linux-x64/bin/node /usr/bin/ \
    && ln -s /opt/node-${NODE_VERSION}-linux-x64/bin/npx /usr/bin/

# ANDROID SDK
# INSPIRED FROM https://www.coveros.com/running-android-tests-in-docker/

ARG ANDROID_SDK_VERSION=4333796
ENV ANDROID_SDK_VERSION=$ANDROID_SDK_VERSION
ARG ANDROID_PLATFORM="android-29"
ARG BUILD_TOOLS="29.0.0"
ENV ANDROID_PLATFORM=$ANDROID_PLATFORM
ENV BUILD_TOOLS=$BUILD_TOOLS

RUN apt install -y openjdk-8-jdk \
    && mkdir -p /opt/adk \
    && wget -q https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_VERSION}.zip \
    && unzip -q sdk-tools-linux-${ANDROID_SDK_VERSION}.zip -d /opt/adk \
    && rm sdk-tools-linux-${ANDROID_SDK_VERSION}.zip \
    && wget -q https://dl.google.com/android/repository/platform-tools-latest-linux.zip \
    && unzip -q platform-tools-latest-linux.zip -d /opt/adk \
    && rm platform-tools-latest-linux.zip \
    && yes | /opt/adk/tools/bin/sdkmanager --licenses \
    && /opt/adk/tools/bin/sdkmanager "build-tools;${BUILD_TOOLS}" "platforms;${ANDROID_PLATFORM}" "ndk-bundle" "lldb;3.1" "cmake;3.10.2.4988404" \
    && ln -s /opt/adk/tools/emulator /usr/bin \
    && ln -s /opt/adk/platform-tools/adb /usr/bin \
    && chown -R 1000:1000 /opt/adk
ENV ANDROID_HOME /opt/adk

# FLUTTER

ARG FLUTTER_VERSION=v1.9.1+hotfix.6
ENV FLUTTER_VERSION=$FLUTTER_VERSION

RUN curl -O https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_${FLUTTER_VERSION}-stable.tar.xz \
    && tar -xf flutter_linux_${FLUTTER_VERSION}-stable.tar.xz -C /opt/ \
    && rm flutter_linux_${FLUTTER_VERSION}-stable.tar.xz \
    && ln -s /opt/flutter/bin/flutter /usr/bin/ \
    && chown -R 1000:1000 /opt/flutter

# DART PROTOBUF

ARG PROTOBUF_VERSION=3.11.2
ENV PROTOBUF_VERSION=$PROTOBUF_VERSION

RUN cd /tmp && \
    wget https://github.com/protocolbuffers/protobuf/releases/download/v$PROTOBUF_VERSION/protoc-$PROTOBUF_VERSION-linux-x86_64.zip && \
    unzip protoc-$PROTOBUF_VERSION-linux-x86_64.zip && \
    mv include/google /usr/local/include/ && \
    mv bin/protoc /usr/local/bin/ && \
    rm -Rf ./protoc-* ./include ./bin

# ADD CUSTOM FILES AND SETTINGS

ADD ./etc/skel/.profile /etc/skel/.profile
ADD ./etc/skel/.vimrc /etc/skel/.vimrc
ADD ./etc/skel/.config /etc/skel/.config
ADD ./start.sh /usr/share/dx11/start.sh

RUN chmod 755 /usr/share/dx11 /usr/share/dx11/start.sh

# MAIN

CMD ["/usr/share/dx11/start.sh"]
