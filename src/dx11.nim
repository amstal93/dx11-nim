import system
import os
import commandeer

import help
import sessionNew
import sessionRun
import sessionExec
import sessionRebuild
import sessionDelete
import sessionSet

when defined(linux):
   import distros
   foreignDep "docker"
   foreignDep "sudo"
   foreignDep "which"
   foreignDep "ip"

# Examples:
#
# dx11 new personal Dockerfile
# dx11 new dev Dockerfile --tor --encrypted --screen=3
# dx11 run personal
# dx11 exec personal <cmd>
# dx11 list
# dx11 rebuild personal   (rerun docker)
# dx11 set personal --screen=4
# dx11 set personal --tor=false
# dx11 delete dev
# dx11 prune

commandline:
   subcommand newSession, "new":
      argument newSessionName, string
      argument newDockerFile, string
      option newUseTor, bool, "tor", "t", false
      option newEncryptData, bool, "encrypt", "e", false
      exitoption "help", "h", getNewHelp()
   subcommand run, "run":
      argument runSessionName, string
      exitoption "help", "h", getRunHelp()
   subcommand exec, "exec":
      argument execSessionName, string
      arguments execCommand, string, false
      exitoption "help", "h", getExecHelp()
   subcommand list, "list":
      exitoption "help", "h", getHelp()
   subcommand rebuild, "rebuild":
      argument rebuildSessionName, string
      exitoption "help", "h", getRebuildHelp()
   subcommand setSessionParams, "set":
      argument setSessionName, string
      option setUseTor, bool, "tor", "t", false
      exitoption "help", "h", getSetHelp()
   subcommand delete, "delete":
      argument deleteSessionName, string
      exitoption "help", "h", getDeleteHelp()
   subcommand prune, "prune":
      exitoption "help", "h", getHelp()
   exitoption "help", "h", getHelp()

proc main =
   if newSession:
      if not sessionNew.newSession(newSessionName, newDockerFile,
            newUseTor, newEncryptData): system.quit(1)
   elif run:
      if not runSession(runSessionName):
         system.quit(1)
   elif exec:
      if not execOnSession(execSessionName, execCommand):
         system.quit(1)
   elif list:
      if execShellCmd("ls ~/.local/share/dx11") != 0:
         system.quit(1)
   elif rebuild:
      if not rebuildSession(rebuildSessionName):
         system.quit(1)
   elif setSessionParams:
      if not setSession(setSessionName, setUseTor):
         system.quit(1)
   elif delete:
      if not deleteSession(deleteSessionName):
         system.quit(1)
   elif prune:
      if execShellCmd("sudo docker image prune") != 0:
         system.quit(1)

   else: echo getHelp()

main()
