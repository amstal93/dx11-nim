import os
import osproc
import distros
import common

# DEPENDENCY AVAILABILITY

proc hasDocker: bool =
    var exitStatus = execCmd("which docker > /dev/null")
    return exitStatus == 0

proc hasX11Docker: bool =
    var exitStatus = execCmd("which x11docker > /dev/null")
    return exitStatus == 0

proc hasEcryptfsSimple: bool =
    var exitStatus = execCmd("which ecryptfs-simple > /dev/null")
    return exitStatus == 0

# INSTALL

const x11DockerSetupCommands = "curl -fsSL https://raw.githubusercontent.com/mviereck/x11docker/master/x11docker | sudo bash -s -- --update"

proc installX11Docker: bool =
    var exitStatus = execShellCmd(x11DockerSetupCommands)
    if exitStatus != 0: return false

    return hasX11Docker()

proc installEcryptfsSimple: bool =
    var setupCommands = ""
    if detectOs(Ubuntu):
        foreignDep "wget"
        setupCommands = """ls /etc/apt/sources.list.d/home:obs_mhogomchungu.list || {
            sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/obs_mhogomchungu/xUbuntu_19.04/ /' > /etc/apt/sources.list.d/home:obs_mhogomchungu.list"
        }
        wget -nv https://download.opensuse.org/repositories/home:obs_mhogomchungu/xUbuntu_19.04/Release.key -O- | sudo apt-key add - 
        sudo apt update
        sudo apt install -y ecryptfs-simple"""
    if detectOs(Debian) or detectOs(Elementary) or detectOs(KNOPPIX) or
            detectOs(SteamOS):
        setupCommands = """ls /etc/apt/sources.list.d/home:obs_mhogomchungu.list || {
            sudo sh -c "echo 'deb http://download.opensuse.org/repositories/home:/obs_mhogomchungu/Debian_9.0/ /' | sudo tee /etc/apt/sources.list.d/home:obs_mhogomchungu.list"
        }
        wget -nv https://download.opensuse.org/repositories/home:obs_mhogomchungu/Debian_9.0/Release.key -O- | sudo apt-key add -
        sudo apt update
        sudo apt install -y ecryptfs-simple"""
    elif detectOs(Gentoo):
        setupCommands = "sudo emerge install ecryptfs-simple"
    elif detectOs(Fedora):
        setupCommands = """sudo dnf config-manager --add-repo https://download.opensuse.org/repositories/home:obs_mhogomchungu/Fedora_30/home:obs_mhogomchungu.repo
            sudo dnf install ecryptfs-simple"""
    elif detectOs(RedHat):
        setupCommands = "sudo rpm install ecryptfs-simple"
    elif detectOs(OpenSUSE):
        setupCommands = """sudo zypper addrepo https://download.opensuse.org/repositories/home:mhogomchungu/openSUSE_Factory/home:mhogomchungu.repo
            sudo zypper refresh
            sudo zypper install ecryptfs-simple"""
    elif detectOs(Slackware):
        setupCommands = "sudo installpkg ecryptfs-simple"
    elif detectOs(OpenMandriva):
        setupCommands = "sudo urpmi ecryptfs-simple"
    elif detectOs(ZenWalk):
        setupCommands = "sudo netpkg install ecryptfs-simple"
    elif detectOs(NixOS):
        setupCommands = "sudo nix-env ecryptfs-simple"
    elif detectOs(Solaris):
        setupCommands = "sudo pkg install ecryptfs-simple"
    elif detectOs(PCLinuxOS):
        setupCommands = "sudo rpm -ivh ecryptfs-simple"
    elif detectOs(ArchLinux) or detectOs(Manjaro):
        setupCommands = "sudo pacman -S ecryptfs-simple"
    else:
        echo "Unsupported Linux distribution"
        return false

    var exitStatus = execShellCmd(setupCommands)
    if exitStatus != 0: return false

    return hasEcryptfsSimple()


# PROMPTS

proc promptForDocker =
    echo """To run dx11 you need to install Docker on your system.

Please, check the install instructions for your distro:
https://docs.docker.com/install/

"""

proc promptX11DockerInstall: bool =
    let promptText = """To run dx11 you need x11docker installed on your system.

Do you want to install it now?"""
    if not confirmAction(promptText, true):
        return false
    return installX11Docker()

proc promptEcryptfsSimpleInstall: bool =
    let promptText =  """To run dx11 with data encryption you need ecryptfs-simple installed on your system.

Do you want to install it now?"""

    if not confirmAction(promptText, true):
        return false
    return installEcryptfsSimple()

# CHECK IF THE ENVIRONMENT IS GOOD TO GO

proc checkSetup*(needsEncryption: bool): bool =
    when not defined(linux):
        echo "DX11 only runs on Linux systems"
        system.quit(1)

    if not hasDocker():
        promptForDocker()
        return false

    if not hasX11Docker():
        let success = promptX11DockerInstall()
        if not success: return false

    if needsEncryption:
        if not hasEcryptfsSimple():
            let success = promptEcryptfsSimpleInstall()
            if not success: return false

    return true
